package middle_Project;

import java.util.Scanner;

public class Mainmbs {
	public static void main(String[]args) {
		MbSArrayList mbs_al = new MbSArrayList();
		Scanner sc = new Scanner(System.in);
		int menu;
		
		while(true) {
			System.out.println("\n****회원관리 프로그램****\n");
			System.out.println("1. 회원가입");
			System.out.println("2. 회원목록");
			System.out.println("3. 회원검색");
			System.out.println("4. 회원정보 수정");
			System.out.println("5. 회원 삭제");
			System.out.println("0.종료");
			
			menu = Integer.parseInt(sc.nextLine());
			  
			switch(menu){
			
				case 1:
					//회원가입
					mbs_al.addMember();
					break;
				case 2://회원 목록
					mbs_al.selectMember();
					break;
				case 3://회원검색
					mbs_al.searchmember();
					break;
				case 4://회원정보수정
					mbs_al. reinformaton();
					break;
				case 5://회원삭제
					mbs_al.removemember();
					break;
				case 0:
					//종료
					System.out.print("프로그램을 종료합니다");	
					System.exit(0);
					sc.close();
					break;
				default:
					System.out.println("잘못된 입력입니다. 다시 선택해주세요");
					
			}
		}
	}

}

