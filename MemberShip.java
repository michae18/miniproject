package middle_Project;


	//회원명,회원번호,
public class MemberShip {
	String membername;
	int memberId;
	
	public MemberShip(String membername,int memberId) {
		this.membername = membername;
		this.memberId = memberId;
	}
	
	public void setmembername(String membername) {
		this.membername = membername;
	}
	public String getmembername() {
		return membername;
	}
	public void setmemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getmemberId() {
		return memberId;
	}
	public String toString() {
		return membername + "님의 아이디는" + memberId + "입니다";
	}

}



