package middle_Project;

import java.util.Scanner;
import java.util.ArrayList;


public class MbSArrayList {
	private ArrayList<MemberShip> ml;
	private Scanner sc;
	
		
	public MbSArrayList() {  
		 ml = new ArrayList<MemberShip>();
		 sc = new Scanner(System.in);
	}
	 

	
		//회원 추가
	 public void addMember() {
		    System.out.print("이름: ");
		    String membername = sc.nextLine();
		    System.out.print("아이디: ");
		    int memberid = sc.nextInt();
		    sc.nextLine();
		    MemberShip membership = new MemberShip(membername, memberid);
		    ml.add(membership);
		    
		    System.out.println("회원이 등록되었습니다.");
		}
	// 회원목록
		public void selectMember() {
			System.out.print("회원목록\n");
			 if(ml.isEmpty()) {
				 System.out.println("등록되어있지 않습니다");
			 }else {
				 for(MemberShip membership: ml) {
					 System.out.println("******************************");
					 System.out.println("이름: " + membership.getmembername());
					 System.out.println("아이디 :" + membership.getmemberId());
				 }
			 }
		}
		
	//회원수정
	public void reinformaton() {
		    System.out.print("수정할 이름 입력: \n");
		    String membername = sc.nextLine();

		    MemberShip membership = getMemberByName(membername);
		    if (membership != null) {
		        System.out.println("*************************");
		        System.out.print("새로운 회원정보를 입력하세요\n");
		        System.out.print("이름: ");
		        membership.setmembername(sc.nextLine());
		        System.out.print("아이디: ");
		        membership.setmemberId(sc.nextInt());
		        sc.nextLine(); // 개행 문자 제거
		        System.out.println("회원 수정이 완료되었습니다");
		    } else {
		        System.out.print("회원정보를 찾을 수 없습니다");
		    }
		}
		
	//회원삭제
	void removemember() {
		System.out.print("삭제할 이름을 입력하세요 :\n");
		String membername = sc.nextLine();
		MemberShip membership = getMemberByName(membername);
		if(membership != null) {
		ml.remove(membership);
		System.out.println("회원이 삭제되었습니다");
		}else {
			System.out.print("회원정보를 찾을 수 없습니다");
				
			}
		}
	public MemberShip getMemberByName(String membername) {
		for(MemberShip membership : ml) {
			if(membership.getmembername().equals(membername)) {
				return membership;
			}
		}
		return null;
	}
		//회원 검색
	public void searchmember() {
		    System.out.print("검색할 이름을 입력하시오\n");
		    String membername = sc.nextLine();

		    MemberShip membership = getMemberByName(membername);
		    if (membership != null) {
		        System.out.println("이름: " + membership.getmembername());
		        System.out.println("아이디: " + membership.getmemberId());
		    } else {
		        System.out.print("회원정보를 찾을 수 없습니다");
		    }
		}
	}

	