package middle_Project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
 class javaswing extends JFrame {
    private ArrayList<MemberShip> memberList;
    private JTextArea outputTextArea;
    private JTextField nameTextField;
    private JTextField idTextField;

    public javaswing() {
        memberList = new ArrayList<>();

        JPanel panel = new JPanel();

        JLabel nameLabel = new JLabel("이름: ");
        nameTextField = new JTextField(20);

        JLabel idLabel = new JLabel("아이디: ");
        idTextField = new JTextField(20);

        JButton addMember = new JButton("회원 추가");
        addMember.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String memberName = nameTextField.getText();
                int memberId = Integer.parseInt(idTextField.getText());

                MemberShip member = new MemberShip(memberName, memberId);
                memberList.add(member);

                outputTextArea.append("회원이 등록되었습니다.\n");
                nameTextField.setText("");
                idTextField.setText("");
            }
        });

        JButton selectMember = new JButton("회원 목록");
        selectMember.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                outputTextArea.setText("");
                for (MemberShip member : memberList) {
                    outputTextArea.append(member.toString() + "\n");
                }
            }
        });

        JButton searchmember = new JButton("회원 검색");
        searchmember.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String searchName = JOptionPane.showInputDialog("검색할 회원 이름을 입력하세요:");
                outputTextArea.setText("");
                boolean found = false;
                for (MemberShip member : memberList) {
                    if (member.getmembername().equals(searchName)) {
                        outputTextArea.append(member.toString() + "\n");
                        found = true;
                    }
                }
                if (!found) {
                    outputTextArea.append("해당 회원을 찾을 수 없습니다.\n");
                }
            }
        });

        JButton reinformaton = new JButton("회원 정보 수정");
        reinformaton.addActionListener(new ActionListener() {
        	 public void actionPerformed(ActionEvent e) {
        	        String searchName = JOptionPane.showInputDialog("수정할 회원 이름을 입력하세요:");
        	        outputTextArea.setText("");
        	        boolean found = false;
        	        for (MemberShip member : memberList) {
        	            if (member.getmembername().equals(searchName)) {
        	                String newName = JOptionPane.showInputDialog("새로운 이름을 입력하세요:");
        	                member.setmembername(newName);
        	                
        	                String newIdString = JOptionPane.showInputDialog("새로운 아이디를 입력하세요:");
        	                int newId = Integer.parseInt(newIdString);
        	                member.setmemberId(newId);
        	                
        	                outputTextArea.append("회원 정보가 수정되었습니다.\n");
        	                found = true;
        	                break;
        	            }
        	        }
        	        if (!found) {
        	            outputTextArea.append("해당 회원을 찾을 수 없습니다.\n");
        	        }
        	    }
        	});
        
        JButton removemember = new JButton("회원 삭제");
        removemember.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String searchName = JOptionPane.showInputDialog("삭제할 회원 이름을 입력하세요:");
                outputTextArea.setText("");
                boolean found = false;
                for (MemberShip member : memberList) {
                    if (member.getmembername().equals(searchName)) {
                        memberList.remove(member);
                        outputTextArea.append("회원이 삭제되었습니다.\n");
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    outputTextArea.append("해당 회원을 찾을 수 없습니다.\n");
                }
            }
        });

        JButton exitButton = new JButton("종료");
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        outputTextArea = new JTextArea(10, 30);
        outputTextArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(outputTextArea);

        panel.add(nameLabel);
        panel.add(nameTextField);
        panel.add(idLabel);
        panel.add(idTextField);
        panel.add(addMember);
        panel.add(selectMember);
        panel.add(searchmember);
        panel.add(reinformaton);
        panel.add(removemember);
        panel.add(exitButton);

        add(panel);
        add(scrollPane, BorderLayout.SOUTH);

        setTitle("회원 관리 프로그램");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new javaswing();
            }
        });
    }
}